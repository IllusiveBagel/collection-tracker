FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.1-stretch AS build
WORKDIR /src
COPY ["CT.Web/CT.Web.csproj", "CT.Web/"]
RUN dotnet restore "CT.Web/CT.Web.csproj"
COPY . .
WORKDIR "/src/CT.Web"
RUN dotnet build "CT.Web.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "CT.Web.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "CT.Web.dll"]